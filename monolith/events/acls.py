from .keys import WEATHER_API_KEY, PICTURE_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PICTURE_API_KEY}
    params = {"query": city + " " + state, "per_page": 1}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city + "," + state + "," + "USA",
        "appid": WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except:
        return {"temperature": None, "description": None}

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        return {
            "temperature": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except:
        return {"temperature": None, "description": None}
